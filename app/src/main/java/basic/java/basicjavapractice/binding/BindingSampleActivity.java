package basic.java.basicjavapractice.binding;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import basic.java.basicjavapractice.R;

public class BindingSampleActivity extends AppCompatActivity implements View.OnClickListener {
    TextView showName;
    Button changeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding_sample);
        bind();
    }

    void bind() {
//        TextView showName = findViewById(R.id.showName);
//        ((TextView) findViewById(R.id.showName)).setText("Cast parametr");
        showName = findViewById(R.id.showName);
        showName.setText("My Name is soheil");
        changeText = findViewById(R.id.changeText);

//        changeText.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showName.setText("Hello Changer");
//            }
//        });
        changeText.setOnClickListener(this);
//        changeText.setOnClickListener(V->{
//            Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();
//        });

    }

    @Override
    public void onClick(View view) {
        Toast.makeText(this, "Hello", Toast.LENGTH_SHORT).show();

    }
}
