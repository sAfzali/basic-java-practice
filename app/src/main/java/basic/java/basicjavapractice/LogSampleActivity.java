package basic.java.basicjavapractice;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;

public class LogSampleActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_sample);
        Log.d("monitor_tag", "onCreate: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d("monitor_tag", "onStart: ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d("monitor_tag", "onRestart: ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("monitor_tag", "onResume: ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("monitor_tag", "onPause: ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("monitor_tag", "onStop: ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("monitor_tag", "onDestroy: ");
    }
}
