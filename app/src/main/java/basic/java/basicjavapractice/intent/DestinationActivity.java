package basic.java.basicjavapractice.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

import basic.java.basicjavapractice.R;

public class DestinationActivity extends AppCompatActivity {
    TextView details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_destination);
        bind();


    }

    void bind() {
        details = findViewById(R.id.details);
        String name = getIntent().getStringExtra("name");
        String family = getIntent().getStringExtra("family");
        int age = getIntent().getIntExtra("age", 0);
        boolean isIranian = getIntent().getBooleanExtra("isIranian", false);
        details.setText(name + " " + family
                + age + isIranian);

    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
    }
}
