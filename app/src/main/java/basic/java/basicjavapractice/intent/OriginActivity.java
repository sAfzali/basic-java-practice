package basic.java.basicjavapractice.intent;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import basic.java.basicjavapractice.R;

public class OriginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText enterName;
    EditText enterFamily;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_origin);
        bind();
    }

    void bind() {
        enterName = findViewById(R.id.enterName);
        enterFamily = findViewById(R.id.enterFamily);
        findViewById(R.id.showDetails).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        String nameValue = enterName.getText().toString();
        String familyValue = enterFamily.getText().toString();
        Intent intent = new Intent(this, DestinationActivity.class);
        intent.putExtra("name", nameValue);
        intent.putExtra("family", familyValue);
        intent.putExtra("age", 45);
        intent.putExtra("isIranian", true);
        startActivity(intent);
    }
}
